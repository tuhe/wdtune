#!/bin/bash

#SBATCH -J wdjob_4_20_py_epoch_3_grid_9

#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH -e grid/wdjob_4_20_py_epoch_3/wdjob_4_20_py_epoch_3_grid_9.err
#SBATCH -o grid/wdjob_4_20_py_epoch_3/wdjob_4_20_py_epoch_3_grid_9.out
module load tensorflow/1.5.0-cp36

set -eo pipefail -o nounset


###
srun python3 -u wdjob_4_20_py_epoch_3_grid.py -n 9 -save_path logs/wdjob_4_20_py_epoch_3/job_9/