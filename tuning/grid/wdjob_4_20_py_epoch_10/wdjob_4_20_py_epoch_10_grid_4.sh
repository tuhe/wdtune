#!/bin/bash

#SBATCH -J wdjob_4_20_py_epoch_10_grid_4

#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH -e grid/wdjob_4_20_py_epoch_10/wdjob_4_20_py_epoch_10_grid_4.err
#SBATCH -o grid/wdjob_4_20_py_epoch_10/wdjob_4_20_py_epoch_10_grid_4.out
module load tensorflow/1.5.0-cp36

set -eo pipefail -o nounset


###
srun python3 -u wdjob_4_20_py_epoch_10_grid.py -n 4 -save_path logs/wdjob_4_20_py_epoch_10/job_4/