#!/bin/bash

#SBATCH -J wdjob_3_20_py_epoch_16_grid_2

#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH -e grid/wdjob_3_20_py_epoch_16/wdjob_3_20_py_epoch_16_grid_2.err
#SBATCH -o grid/wdjob_3_20_py_epoch_16/wdjob_3_20_py_epoch_16_grid_2.out
module load tensorflow/1.5.0-cp36

set -eo pipefail -o nounset


###
srun python3 -u wdjob_3_20_py_epoch_16_grid.py -n 2 -save_path logs/wdjob_3_20_py_epoch_16/job_2/