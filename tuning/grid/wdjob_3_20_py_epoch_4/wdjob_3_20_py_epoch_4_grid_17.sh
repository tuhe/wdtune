#!/bin/bash

#SBATCH -J wdjob_3_20_py_epoch_4_grid_17

#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH -e grid/wdjob_3_20_py_epoch_4/wdjob_3_20_py_epoch_4_grid_17.err
#SBATCH -o grid/wdjob_3_20_py_epoch_4/wdjob_3_20_py_epoch_4_grid_17.out
module load tensorflow/1.5.0-cp36

set -eo pipefail -o nounset


###
srun python3 -u wdjob_3_20_py_epoch_4_grid.py -n 17 -save_path logs/wdjob_3_20_py_epoch_4/job_17/