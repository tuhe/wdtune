import os
import sys
# import dill
import numpy as np
import os.path as op

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
# Bayesian tuning
from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import uniform, choice

def data():
    # Generate dummy data
    data_dir = '../split'
    x_train = np.load(op.join(data_dir, 'train/nothing,nothing,hcp_mlp505050.npy'))
    y_train = np.load(op.join(data_dir, 'train_labels/hcp_mlp505050.npy'))
    x_test = np.load(op.join(data_dir, 'test/nothing,nothing,hcp_mlp505050.npy'))
    y_test = np.load(op.join(data_dir, 'test_labels/hcp_mlp505050.npy'))
    return x_train, y_train, x_test, y_test


def create_model(I_train,I_test, params):
    x,y = all_data()
    x_train = x[I_train]
    y_train = y[I_train]
    x_test = x[I_test]
    y_test = y[I_test]

        # , y_train, x_test, y_test

    model = Sequential()
    activ = 'relu' #{{choice(['elu', 'relu', 'tanh'])}}
    batchnorm = True #{{choice([True, False])}}
    batch_size = 64 # {{choice([16, 32, 64])}}

    model.add(Dense( int( space['dense1']),
        input_dim=34716, activation=activ))
    model.add(Dropout({{uniform(0, 0.5)}}))
    if batchnorm:
        model.add(keras.layers.BatchNormalization())

    model.add(Dense({{choice([16, 64, 256])}},
        activation=activ))
    model.add(Dropout({{uniform(0, 0.5)}}))
    if batchnorm:
        model.add(keras.layers.BatchNormalization())

    if {{choice([False, True])}}: # use third layer?
        model.add(Dense({{choice([16, 64, 256])}},
            activation=activ))
        model.add(Dropout({{uniform(0, 0.5)}}))
        if batchnorm:
            model.add(keras.layers.BatchNormalization())

    if {{choice([False, True])}}: # use fourth layer?
        model.add(Dense({{choice([16, 64, 256])}},
            activation=activ))
        model.add(Dropout({{uniform(0, 0.5)}}))
        if batchnorm:
            model.add(keras.layers.BatchNormalization())

    model.add(Dense(2, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr={{choice([1e-2, 1e-3, 1e-4])}}),
                  metrics=['accuracy'])

    model.fit(x_train, y_train, epochs=50, batch_size=batch_size)
    score, acc = model.evaluate(x_test, y_test, batch_size=batch_size)
    print("Test accuracy: {}".format(acc))
    return {'loss': -acc, 'status': STATUS_OK, 'model': model}

def all_data():
    x_train, y_train, x_test, y_test = data()
    x = np.stack( [x_train,x_test],axis=0)
    y = np.stack([y_train, y_test], axis=0)
    return x,y

if __name__ == '__main__':

    s = optim.get_hyperopt_model_string(model=create_model,data=data,functions=None,notebook_name=None,verbose=False,stack=3)
    best_run, best_model = optim.minimize(model=create_model,
            data=data, algo=tpe.suggest, max_evals=100, trials=Trials())
    x_train, y_train, x_test, y_test = data()
    print("Evalutation of best model:")
    print(best_model.evaluate(x_test, y_test))
    print("Best run")
    print(best_run)
    best_model.save("best_model.hdf5")
