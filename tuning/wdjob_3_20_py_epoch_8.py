
import os
import sys
if not sys.version == "nt":
    sys.path.append("../../")

import numpy as np
import os.path as op
import keras
import thtools
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
from thtools.farm import blackbox_grid, GPyOptParameterGenerator, make_2_level_CV
# BBSweepParameterGenerator,
# BBGPParameterGenerator,
# LatinParameterGenerator,

def data():
    # Generate dummy data
    data_dir = '../split'
    x_train = np.load(op.join(data_dir, 'train/nothing,nothing,hcp_mlp505050.npy'))
    y_train = np.load(op.join(data_dir, 'train_labels/hcp_mlp505050.npy'))
    x_test = np.load(op.join(data_dir, 'test/nothing,nothing,hcp_mlp505050.npy'))
    y_test = np.load(op.join(data_dir, 'test_labels/hcp_mlp505050.npy'))
    return x_train, y_train, x_test, y_test

def all_data():
    x_train, y_train, x_test, y_test = data()
    x = np.concatenate( [x_train,x_test], axis=0)
    y = np.concatenate([y_train, y_test], axis=0)
    return x, y

STEPS = 40
regularization = np.logspace(np.log10(0.001), np.log10(1), STEPS)
space_wide = {'dense1': np.linspace(16,64,STEPS),
             'dropout1': np.linspace(16,64,STEPS),
             'regularizer': regularization}

def create_model(I_train,I_test, params_all):
    US = 5
    E_train = []
    E_test = []

    for k in params_all:
        params_all[k] = space_wide[k][ int(params_all[k])]

    if "dropout2" not in params_all:
        params_all["dropout2"] = params_all["dropout1"]

    if "dense2" not in params_all:
        params_all["dense2"] = params_all["dense1"]

    regularization[0] = 0

    for u in range(US):
        pdef = {'dense1' : 32, 'dropout1': 0.3, 'dense2':32, 'dropout2': 32, 'lr':0.001, 'regularization':0} # default values
        pdef.update(params_all)
        params = pdef
        reg = keras.regularizers.l2(regularization[ int(params['regularization']) ] )

        x, y = all_data()
        x_train = x[I_train]
        y_train = y[I_train]
        x_test = x[I_test]
        y_test = y[I_test]

        dense1 = int( params['dense1'])
        dropout1 = params['dropout1']

        dense2 = int(params['dense2'])
        dropout2 = params['dropout2']
        lr = params['lr']

        model = Sequential()
        activ = 'relu' #{{choice(['elu', 'relu', 'tanh'])}}
        batchnorm = True #{{choice([True, False])}}
        batch_size = 64 # {{choice([16, 32, 64])}}
        thirdlayer = True

        model.add(Dense(dense1,
            input_dim=34716, activation=activ, kernel_regularizer=reg))
        model.add(Dropout(dropout1))
        if batchnorm:
            model.add(keras.layers.BatchNormalization())

        model.add(Dense(dense1,
            activation=activ, kernel_regularizer=reg))
        model.add(Dropout(dropout1))
        if batchnorm:
            model.add(keras.layers.BatchNormalization())

        if thirdlayer: # use third layer?
            model.add(Dense(dense2,
                activation=activ, kernel_regularizer=reg))
            model.add(Dropout(dropout2))
            if batchnorm:
                model.add(keras.layers.BatchNormalization())

        if False: # use fourth layer?
            model.add(Dense(dense2,
                activation=activ))
            model.add(Dropout(dropout2))
            if batchnorm:
                model.add(keras.layers.BatchNormalization())

        model.add(Dense(2, activation='softmax'))

        model.compile(loss='categorical_crossentropy',
                      optimizer=Adam(lr=lr),
                      metrics=['accuracy'])

        model.fit(x_train, y_train, epochs=100, batch_size=batch_size)
        score_test, acc_test = model.evaluate(x_test, y_test, batch_size=batch_size)
        score_train, acc_train = model.evaluate(x_train, y_train, batch_size=batch_size)

        E_train.append(1-acc_train)
        E_test.append(1-acc_test)

    E_train = np.mean(E_train)
    E_test = np.mean(E_test)

    return E_train,E_test,{} #{'loss': -acc_test, 'status': STATUS_OK, 'model': model}

params = {'dense1' : 32, 'dropout1': 0.3, 'dense2':32, 'dropout2': 32, 'lr':0.001, "regularization":10}

x,y = all_data()
n = x.shape[0]
K1_val = 5
K2_traintest = 5
# n = 25
CV_splits = make_2_level_CV(K1_val=K1_val,K2_traintest=K2_traintest, N=n, randomize=True)

#space = {k: [np.min(space_wide[k]), np.max(space_wide[k])] for k in space_wide}
space = {k: [0, len(space_wide[k])-1] for k in space_wide}

n_random_points_init = 5

parameter_generator_gpyopt = GPyOptParameterGenerator(space, num_validation_folds=len(CV_splits),
                                                      n_random_points_init=n_random_points_init)
n_epochs_to_launch = 20

jobname = "wdjob_%i_%i"%(len(list(space.keys())), n_epochs_to_launch)

plot_iter = 1
if thtools.is_xstream():
    plot_iter = 10000 # don't try to plot on xstream

elif False:
    I_train = CV_splits[0]['traintest'][0]['train']
    I_test = CV_splits[0]['traintest'][0]['test']
    ms = create_model(I_train, I_test, params)
    print(ms)

if thtools.is_win():
    from thtools.farm.blackbox_plot import blackbox_plot
    blackbox_plot("./wdjob_3_20")
    sys.exit()


# Script for doign cross validation (train, test, validation).
import pickle
from thtools.farm import autogrid
from thtools import is_xstream

with open( "/cstor/stanford/gerritsn/users/herlau/wdtune/tuning/wdjob_3_20/wdjob_3_20_pars_epoch_8.pkl", 'rb') as f:
    bb_conf = pickle.load(f)
    validation_splits = bb_conf['validation_splits']
    CV_splits = bb_conf['CV_splits']
    opts = bb_conf.get('opts', dict())
    I_traintest_val = CV_splits

# <grid_rm>
if is_xstream():
    autogrid(script=None, rs="rs", opts=opts, clean_py = True, push_git = False,
             rs_out="/cstor/stanford/gerritsn/users/herlau/wdtune/tuning/wdjob_3_20/wdjob_3_20_rs_epoch_8.pkl")
# </grid_rm>


rs = {}
rs['gridjob_conf_file'] = "/cstor/stanford/gerritsn/users/herlau/wdtune/tuning/wdjob_3_20/wdjob_3_20_pars_epoch_8.pkl"
rs['gridjob_py_out'] = "/cstor/stanford/gerritsn/users/herlau/wdtune/tuning/wdjob_3_20_py_epoch_8.py"

for i_val in range( len(I_traintest_val) ):
    for j_tt in range( len(I_traintest_val[i_val]['traintest'])+1):
        # <grid_fun>
        I_val = I_traintest_val[i_val]['validation']

        n_traintest = len(I_traintest_val[i_val]['traintest'])

        I_train = I_traintest_val[i_val]['traintest'][j_tt if j_tt < n_traintest else 0]['train']
        I_test = I_traintest_val[i_val]['traintest'][j_tt if j_tt < n_traintest else 0]['test']

        par_tune = validation_splits[i_val]['parameters_tune']
        n_traintest = len(I_traintest_val[i_val]['traintest'])
        if j_tt < n_traintest:
            E_train,E_test,out = create_model(I_train,I_test,par_tune)
            out['label'] = "val sweep %i: I_train(%i) -> I_test(%i)"%(i_val, j_tt, j_tt)
        else:
            I_train = I_train+I_test
            I_test = I_val
            E_train, E_test,out = create_model(I_train, I_test, par_tune)
            out['label'] = "(I_train(all)+I_test(all))-I_val(%i) -> I_val(%i)"%(i_val,i_val)

        out['config'] = bb_conf
        out['E_train'] = E_train
        out['E_test'] = E_test
        out['is_validation'] = j_tt == n_traintest
        rs[(i_val, j_tt)] = out
        # </grid_fun>

if not is_xstream(): # for local debuggin'
    print("Windows: Saving to pickle file...")
    with open("/cstor/stanford/gerritsn/users/herlau/wdtune/tuning/wdjob_3_20/wdjob_3_20_rs_epoch_8.pkl", 'wb') as f:
        pickle.dump(rs, f)

# 